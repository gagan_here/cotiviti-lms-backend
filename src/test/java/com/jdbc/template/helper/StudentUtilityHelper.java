package com.jdbc.template.helper;

import com.jdbc.template.Model.Student;

import java.util.List;

public class StudentUtilityHelper {



    public static Student mockStudent() {
        Student student = new Student();
        student.setStudentid(1L);
        student.setFirstname("Gagan");
        student.setLastname("Basnet");
        student.setEmail("gagan.basnet@cotiviti.com");
        student.setPhone("9816381480");
        student.setAddress("Biratnagar");
        student.setClassname("Bachelor");
        return student;
    }


    public static List<Student> mockStudents() {
        Student student = new Student();

        student.setStudentid(2L);
        student.setFirstname("Manish");
        student.setLastname("Dahal");
        student.setEmail("manish.dahal@cotiviti.com");
        student.setPhone("9817812480");
        student.setAddress("Kathmandu");
        student.setClassname("Master");

        Student student1 = new Student();
        student1.setStudentid(1L);
        student1.setFirstname("Gagan");
        student1.setLastname("Basnet");
        student1.setEmail("gagan.basnet@cotiviti.com");
        student1.setPhone("9816381480");
        student1.setAddress("Biratnagar");
        student1.setClassname("Bachelor");



        return List.of(student, student1);
    }

}
