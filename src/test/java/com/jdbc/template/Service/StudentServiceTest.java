package com.jdbc.template.Service;

import com.jdbc.template.Model.Student;
import com.jdbc.template.Repository.StudentDAO;
import com.jdbc.template.Repository.StudentRepository;
import com.jdbc.template.helper.StudentUtilityHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StudentServiceTest {

    @InjectMocks
    private StudentService studentService;

    @Mock
    private StudentDAO studentDAO;

    private Student student;

    private List<Student> students;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        student = StudentUtilityHelper.mockStudent();
        students = StudentUtilityHelper.mockStudents();

        when(studentDAO.findAll()).thenReturn(students);
        when(studentDAO.findBystudentId(anyLong())).thenReturn(student);
    }

    @Test
    void testGetStudents() {

        // Perform
        List<Student> students = studentService.getStudents();

        // Assert
        assertThat(students).hasSize(2);
        assertThat(students.get(0).getFirstname()).isEqualTo("Manish");
        assertThat(students.get(1).getFirstname()).isEqualTo("Gagan");
    }

    @Test
    void testGetStudent() {

        // Perform
        Student student = studentService.getStudent(1l);

        // Assert
        assertThat(student.getEmail()).isEqualTo("gagan.basnet@cotiviti.com");
    }

}
