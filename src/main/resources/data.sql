-- INSERT INTO user(id, username, password, role)
-- VALUES(1, 'gagan', '$2y$12$MkcZLUZoo9WPtLw8stmereZRycuxN69d2/qd8sejKsKM0vARw/8Ei', 'user');
-- INSERT INTO user(id, username, password, role)
-- VALUES(2, 'admin', '$2y$12$UQaErSCWzaXCsEMFUAX1vOKDDmEDVGLUibVGGzFLrsVoAQC9o0vCS', 'admin');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(1, 'gagan', 'basnet', 'abc@gmail.com', '9816381480', 'brt', '5');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(2, 'manish', 'shah', 'def@gmail.com', '9827314217', 'ith', '7');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(3, 'shweta', 'tiwari', 'xyz@gmail.com', '9714381486', 'ktm', '9');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(4, 'alisa', 'rajbanshi', 'asd@gmail.com', '9875698480', 'bhw', '5');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(5, 'ashok', 'pathak', 'qwe@gmail.com', '9634571025', 'bhk', '8');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(6, 'manjil', 'bhandari', 'mno@gmail.com', '982432680', 'ktm', '6');

INSERT INTO student(studentid, firstname, lastname, email, phone, address, classname)
VALUES(7, 'sachin', 'shrestha', 'ghi@gmail.com', '9817510325', 'brt', '9');


--     private long bookid;
--     private String bookname;
--     private String writer;
--     private String publication;
--     private boolean available;

INSERT INTO books(bookid, bookname, writer, publication, available)
VALUES(1, '2 states', 'Chetan Bhaghat', 'Rupa', true);

INSERT INTO books(bookid, bookname, writer, publication, available)
VALUES(2, 'The fault in our stars', 'John Green', 'Penguin', true);

INSERT INTO books(bookid, bookname, writer, publication, available)
VALUES(3, 'The notebook', 'Nicholas Sparks', 'Grand Central Publishing', true);

INSERT INTO books(bookid, bookname, writer, publication, available)
VALUES(4, 'I too had a love story', 'Ravinder Singh', 'Penguin', true);

