package com.jdbc.template.Service;

import com.jdbc.template.Exception.StudentNotFoundException;
import com.jdbc.template.Model.Books;
import com.jdbc.template.Repository.BookDAO;
import com.jdbc.template.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDAO bookDAO;

    @Autowired
    BookRepository bookRepository;

    public List<Books> getBooks() {
        return bookDAO.findAll();
    }

    public Books getBook(long bookId) {
        return bookDAO.findByBookId(bookId);
    }

    public String addBook(Books book) {

        var byId = bookRepository.findById(book.getBookid());

        if (byId.isPresent()) {
            throw new StudentNotFoundException("Book already exists!", HttpStatus.CONFLICT);
        }

        bookRepository.save(book);

        return "Book added successfully!";
    }

    public String updateBook(Books book) {
        var byId = bookRepository.findById(book.getBookid())
                .orElseThrow(() -> new StudentNotFoundException("Book not found", HttpStatus.NOT_FOUND));

        bookRepository.save(book);

        return "Book updated successfully";
    }

    public String deleteBook(long id) {
        var byId = bookRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Book not found", HttpStatus.NOT_FOUND));
        bookRepository.deleteById(id);
        return "Book deleted successfully";
    }

}
