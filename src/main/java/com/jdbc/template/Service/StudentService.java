package com.jdbc.template.Service;

import com.jdbc.template.Exception.StudentNotFoundException;
import com.jdbc.template.Model.Books;
import com.jdbc.template.Model.Student;
import com.jdbc.template.Repository.StudentDAO;
import com.jdbc.template.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getStudents() {
        return studentDAO.findAll();
    }

    public Student getStudent(long studentId) {
        Student student = studentDAO.findBystudentId(studentId);
        if (Objects.isNull(student)) {
            throw new StudentNotFoundException("Student not found", HttpStatus.NOT_FOUND);
        }
        return student;
    }

    public String addStudent(Student student) {

        var byId = studentRepository.findById(student.getStudentid());

        if (byId.isPresent()) {
            throw new StudentNotFoundException("Student already exists!", HttpStatus.CONFLICT);
        }

        studentRepository.save(student);

        return "Student added successfully!";
    }

    public Student updateStudent(Student student, long id) {
        var byId = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Student not found", HttpStatus.NOT_FOUND));

        studentRepository.save(student);

        return byId;
    }

    public String deleteStudent(long id) {
        var byId = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Student not found", HttpStatus.NOT_FOUND));
        studentRepository.deleteById(id);
        return "Student deleted successfully";
    }

    public Student getStudentByEmail(String email) {
        Student byEmail = studentRepository.findByEmail(email);
        if(Objects.isNull(byEmail)) {
            throw new StudentNotFoundException("Student not found", HttpStatus.NOT_FOUND);
        }

        return byEmail;
    }

    public List<Map<String, Object>> getInfo() {
        return studentDAO.getInfo();
    }

}
