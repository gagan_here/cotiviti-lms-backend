package com.jdbc.template.Controller;

import com.jdbc.template.Exception.BookNotFoundException;
import com.jdbc.template.Exception.StudentNotFoundException;
import com.jdbc.template.Model.ExceptionResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionHandleController {

    @ExceptionHandler(StudentNotFoundException.class)
    public @ResponseBody
    ExceptionResponse studentNotFoundException(final StudentNotFoundException exception, final HttpServletRequest request) {
        return new ExceptionResponse(
                exception.getMessage(),
                request.getRequestURI(),
                exception.getHttpStatus(),
                LocalDateTime.now()
        );
    }


    @ExceptionHandler(BookNotFoundException.class)
    public @ResponseBody
    ExceptionResponse bookNotFoundException(final BookNotFoundException exception, final HttpServletRequest request) {
        return new ExceptionResponse(
                exception.getMessage(),
                request.getRequestURI(),
                exception.getHttpStatus(),
                LocalDateTime.now()
        );
    }
}
