package com.jdbc.template.Controller;

import com.jdbc.template.Model.Books;
import com.jdbc.template.Model.Student;
import com.jdbc.template.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/students")
    public ResponseEntity<List<Student>> getStudents() {
        return ResponseEntity.ok(studentService.getStudents());
    }

    @GetMapping(value = "/students/{studentId}")
    public ResponseEntity<Student> getStudent(@PathVariable long studentId) {
        return ResponseEntity.ok(studentService.getStudent(studentId));
    }

    @GetMapping(value = "/student/{email}")
    public ResponseEntity<Student> getStudentByEmail(@PathVariable String email) {
        return ResponseEntity.ok(studentService.getStudentByEmail(email));
    }

    @PostMapping(value = "/students")
    public ResponseEntity<String> addStudent(@RequestBody Student student) {
        return ResponseEntity.ok(studentService.addStudent(student));

    }

    @PutMapping(value = "/students/{studentId}")
    public ResponseEntity<Student> updateStudent(@RequestBody Student student, @PathVariable long studentId) {
        return ResponseEntity.ok(studentService.updateStudent(student, studentId));
    }

    @DeleteMapping(value = "students/{studentId}")
    public ResponseEntity<String> deleteStudent(@PathVariable long studentId) {
        return ResponseEntity.ok(studentService.deleteStudent(studentId ));
    }


    @GetMapping(value = "info")
    public ResponseEntity<List<Map<String, Object>>> getIngo() {
        return ResponseEntity.ok(studentService.getInfo());
    }

}
