package com.jdbc.template.Controller;

import com.jdbc.template.Model.Books;
import com.jdbc.template.Model.Student;
import com.jdbc.template.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(value = "/books")
    public ResponseEntity<List<Books>> getBooks() {
        return ResponseEntity.ok(bookService.getBooks());
    }

    @GetMapping(value = "/books/{bookId}")
    public ResponseEntity<Books> getBook(@PathVariable long bookId) {
        return ResponseEntity.ok(bookService.getBook(bookId));
    }

    @PostMapping(value = "/books/add")
    public ResponseEntity<String> addBook(@RequestBody Books book) {
        return ResponseEntity.ok(bookService.addBook(book));

    }

    @PutMapping(value = "/books/update")
    public ResponseEntity<String> updateBook(@RequestBody Books book) {
        return ResponseEntity.ok(bookService.updateBook(book));
    }

    @DeleteMapping(value = "books/{bookId}")
    public ResponseEntity<String> deleteBook(@PathVariable long bookId) {
        return ResponseEntity.ok(bookService.deleteBook(bookId));
    }
}
