package com.jdbc.template.Model;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ExceptionResponse {

    private String errorMessage;
    private String requestedURI;
    private HttpStatus httpStatus;
    private LocalDateTime timestamp;

    public ExceptionResponse(String errorMessage, String requestedURI, HttpStatus httpStatus, LocalDateTime timestamp) {
        this.errorMessage = errorMessage;
        this.requestedURI = requestedURI;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRequestedURI() {
        return requestedURI;
    }

    public void setRequestedURI(String requestedURI) {
        this.requestedURI = requestedURI;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
