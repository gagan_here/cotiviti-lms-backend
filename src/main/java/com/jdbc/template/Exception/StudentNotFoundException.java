package com.jdbc.template.Exception;

import org.springframework.http.HttpStatus;

public class StudentNotFoundException extends RuntimeException{
    private String message;
    private HttpStatus httpStatus;

    public StudentNotFoundException(String message, HttpStatus httpStatus) {
        super(message);
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
