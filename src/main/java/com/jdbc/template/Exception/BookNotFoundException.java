package com.jdbc.template.Exception;

import org.springframework.http.HttpStatus;

public class BookNotFoundException extends RuntimeException{
    private String message;
    private HttpStatus httpStatus;

    public BookNotFoundException(String message, HttpStatus httpStatus) {
        super(message);
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
