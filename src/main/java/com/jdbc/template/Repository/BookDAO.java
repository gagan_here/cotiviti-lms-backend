package com.jdbc.template.Repository;

import com.jdbc.template.Mapper.BookRowMapper;
import com.jdbc.template.Model.Books;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Books> findAll() {
        return jdbcTemplate.query("select * from books", new BookRowMapper());
    }

    public Books findByBookId(long bookId) {
        return jdbcTemplate.queryForObject("select * from books where id = ?",
                new BookRowMapper(), bookId);
    }
}
