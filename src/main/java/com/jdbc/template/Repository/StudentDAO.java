package com.jdbc.template.Repository;

import com.jdbc.template.Mapper.BookRowMapper;
import com.jdbc.template.Mapper.StudentRowMapper;
import com.jdbc.template.Model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class StudentDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Student> findAll() {
        return jdbcTemplate.query("select * from student", new StudentRowMapper());
    }

//    private Long studentid;
//    private String firstname;
//    private String lastname;
//    private String email;
//    private String phone;
//    private String address;
//    private String classname;

//    private String bookname;
//    private String writer;
//    private String publication;
//    private Date bookdate;
//    private boolean available;


    public Student findBystudentId(long studentId) {
        return jdbcTemplate.queryForObject("select * from student where studentId = ?",
                new StudentRowMapper(), studentId);
    }

    public List<Map<String, Object>> getInfo() {
        String query ="select s.studentid, s.firstname, s.lastname, s.classname, " +
                "b.bookname, b.writer, b.bookdate " +
                "from student s " +
                "join books b " +
                "on s.studentid = b.studentid";

        List<Map<String, Object>> query1 = jdbcTemplate.queryForList(query, new Object[] {},
                new StudentRowMapper(),
                new BookRowMapper());
        return query1;
    }
}
