package com.jdbc.template.Mapper;

import com.jdbc.template.Model.Books;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class BookRowMapper implements RowMapper<Books> {

    @Override
    public Books mapRow(ResultSet rs, int i) throws SQLException {

        Books books = new Books();
        books.setBookid(rs.getLong("bookid"));
        books.setBookname(rs.getString("bookname"));
        books.setWriter(rs.getString("writer"));
        books.setPublication(rs.getString("publication"));
        books.setAvailable(rs.getBoolean("available"));
        return books;
    }
}
